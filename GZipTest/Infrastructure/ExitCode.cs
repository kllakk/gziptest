﻿namespace GZipTest.Infrastructure
{
    public enum ExitCode 
    {
        Success = 0,
        Error = 1
    }
}
