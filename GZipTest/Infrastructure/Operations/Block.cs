﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GZipTest.Infrastructure.Operations
{
    enum BlockStatus
    {
        Init,
        Read,
        Compressed,
        Decompressed,
        Written
    }

    public class Block
    {
        public static readonly int HeaderBlockIdLength = sizeof(int);
        public static readonly int HeaderBlockSizeLength = sizeof(int);

        public long Id { get; }
        public int Size { get; }
        public byte[] Data { get; }

        public Block(long _id, byte[] _data, int _size)
        {
            Id = _id;
            Data = _data;
            Size = _size;
        }
    }
}
