﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;

namespace GZipTest.Infrastructure.Operations
{
    public class Compress : Operation, IOperation
    {
        static bool hasError = false;
        static int blocksCount = 0;
        static long blocksProceed = 0;
        static BlockStatus[] proceedBlocks;
        
        static object _outSync = new object();
        
        static Queue queueCompress = new Queue();
        static Queue queueWriting = new Queue();        

        public Compress(FileInfo _fileIn, FileInfo _fileOut) : base(_fileIn, _fileOut) { }

        public ExitCode Run()
        {
            blocksCount = (int)Math.Ceiling(fileIn.Length / (decimal)BufferSize);

            proceedBlocks = new BlockStatus[blocksCount];

            int threadsCount = blocksCount < processorsCount ? blocksCount : processorsCount;

            var compressThreads = new Thread[threadsCount];
            var writeThread = new Thread[threadsCount];

            if (compressThreads.Count() > 0)
            {
                log.Trace($"Компрессия (поток чтения основной, потоков компрессии: {compressThreads.Count()}, поток записи 1)");

                // записываем заголовок файла
                var gztHId = Encoding.UTF8.GetBytes(GZTFileHeaderId);
                streamOut.Write(gztHId, 0, gztHId.Length);

                // записываем количество блоков файла
                var gztHBlocks = BitConverter.GetBytes(blocksCount);
                streamOut.Write(gztHBlocks, 0, GZTFileHeaderBlocksSize);

                for (int i = 0; i < threadsCount; i++)
                {
                    compressThreads[i] = new Thread(ThreadCompressing) { Name = $"Поток компрессии {i}" };
                    compressThreads[i].Start();
                    writeThread[i] = new Thread(ThreadWriting) { Name = $"Поток записи {i}" };
                    writeThread[i].Start();
                }

                
                int blockId = 0;
                int read = 0;

                // читаем данные
                do
                {
                    if (_break || cancel())
                    {
                        log.Trace($"{Thread.CurrentThread.Name}: Принудительная остановка");
                        break;
                    }

                    var buffer = new byte[BufferSize];
                    read = streamIn.Read(buffer, 0, buffer.Length);
                    if (read > 0)
                    {
                        proceedBlocks[blockId] = BlockStatus.Read;
                        lock (queueCompress.SyncRoot) queueCompress.Enqueue(new Block(blockId, buffer, read));
#if DEBUG
                        log.Trace($"Read {blockId}/{blocksCount}) {streamIn.Name} ({read}) {Thread.CurrentThread.Name}");
#endif
                        blockId++;
                    }
                }
                while (read > 0);

                foreach (var t in compressThreads)
                {
                    t.Join();
                }

                foreach (var t in writeThread)
                {
                    t.Join();
                }

                if (!hasError && !_break && !cancel())
                {
                    var compressedPercentage = 100 - (streamOut.Length * 100) / fileIn.Length;

                    log.Trace($"Степень сжатия {compressedPercentage}%");

                    exitCode = ExitCode.Success;
                }
            }
            else
            {
                throw new Exception($"Ошибка выделения потоков");
            }

            return exitCode;
        }

        void ThreadCompressing()
        {
            try
            {
                while (proceedBlocks.Any(x => x == BlockStatus.Read || x == BlockStatus.Init))
                {
                    if (_break || cancel())
                    {
                        log.Trace($"{Thread.CurrentThread.Name}: Принудительная остановка");
                        break;
                    }

                    Block block = null;

                    lock (queueCompress.SyncRoot)
                    {
                        if (queueCompress.Count > 0) { 
                            block = queueCompress.Dequeue() as Block;
                        }
                    }

                    if (block != null)
                    {                       
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (GZipStream streamGZip = new GZipStream(memoryStream, CompressionLevel.Optimal))
                            {
                                proceedBlocks[block.Id] = BlockStatus.Compressed;                                
                                streamGZip.Write(block.Data, 0, block.Size);                                
                            }

                            var data = memoryStream.ToArray();
                            var length = data.Length;
                            
                            lock (queueWriting.SyncRoot) queueWriting.Enqueue(new Block(block.Id, data, data.Length));
#if DEBUG
                            log.Trace($"Compressed {block.Id}) {streamOut.Name} {Thread.CurrentThread.Name}");
#endif
                        }
                    }
                }
            }
            catch (Exception e)
            {
                hasError = true;
                log.Fatal($"{Thread.CurrentThread.Name}: {e.Message}");
                log.Trace(e.StackTrace);
                Break();
            }
        }

        void ThreadWriting()
        {
            try
            {
                while (proceedBlocks.Any(x => x == BlockStatus.Compressed || x == BlockStatus.Read || x == BlockStatus.Init))
                {
                    if (_break || cancel())
                    {
                        log.Trace($"{Thread.CurrentThread.Name}: Принудительная остановка");
                        break;
                    }

                    Block block = null;

                    lock (queueWriting.SyncRoot)
                    {
                        if (queueWriting.Count > 0)
                        {
                            block = queueWriting.Dequeue() as Block;
                        }
                    }

                    if (block != null)
                    {
                        lock (_outSync)
                        {
                            proceedBlocks[block.Id] = BlockStatus.Written;

                            // записываем номер блока
                            var blockId = BitConverter.GetBytes(block.Id);
                            streamOut.Write(blockId, 0, Block.HeaderBlockIdLength);

                            // записываем размер сжатого блока
                            var blockSize = BitConverter.GetBytes(block.Size);
                            streamOut.Write(blockSize, 0, Block.HeaderBlockSizeLength);

                            // записываем данные                            
                            streamOut.Write(block.Data, 0, block.Size);
                        }
#if DEBUG
                        log.Trace($"Written {block.Id}) {streamOut.Name} ({block.Size}) {Thread.CurrentThread.Name}");
#endif
                        progress?.Invoke(Interlocked.Increment(ref blocksProceed), blocksCount);
                    }
                }
            }
            catch (Exception e)
            {
                hasError = true;
                log.Fatal($"{Thread.CurrentThread.Name}: {e.Message}");
                log.Trace(e.StackTrace);
                Break();
            }
        }
    }
}
