﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;

namespace GZipTest.Infrastructure.Operations
{
    public class Decompress : Operation, IOperation
    {
        static int blocksCount = 0;
        static long blocksProceed = 0;
        static long blocksNext = 0;
        static bool hasError = false;
        static BlockStatus[] proceedBlocks;

        static object _queueWritingSync = new object();
        static Queue queueDecompress = new Queue();
        static Dictionary<long, Block> queueWriting = new Dictionary<long, Block>();
        
        public Decompress(FileInfo _fileIn, FileInfo _fileOut) : base(_fileIn, _fileOut) { }

        public ExitCode Run()
        {
            // считываем заголовок файла
            var gztHId = Encoding.UTF8.GetBytes(GZTFileHeaderId);
            var gztHIdFileIn = new byte[gztHId.Length];
            int readHId = streamIn.Read(gztHIdFileIn, 0, gztHId.Length);
            if (readHId == gztHId.Length && gztHId.SequenceEqual(gztHIdFileIn)) {

                var blocks = new byte[GZTFileHeaderBlocksSize];
                int readHBlocks = streamIn.Read(blocks, 0, blocks.Length);
                if (readHBlocks > 0)
                {
                    blocksCount = BitConverter.ToInt32(blocks, 0);

                    proceedBlocks = new BlockStatus[blocksCount];

                    int threadsCount = blocksCount < processorsCount ? blocksCount : processorsCount;
                    
                    log.Trace($"Декомпрессия (поток чтения основной, потоков записи: {threadsCount})");
                    
                    var decompressThreads = new Thread[threadsCount];
                    var writeThread = new Thread(ThreadWriting) { Name = $"Поток записи", Priority = ThreadPriority.AboveNormal };

                    for (int i = 0; i < threadsCount; i++)
                    {
                        decompressThreads[i] = new Thread(ThreadDecompressing) { Name = $"Поток декомпрессии {i}" };
                    }

                    foreach (var t in decompressThreads)
                    {
                        t.Start();
                    }

                    writeThread.Start();

                    int read = 0;
                    do
                    {
                        if (_break || cancel())
                        {
                            log.Trace($"{Thread.CurrentThread.Name}: Принудительная остановка");
                            break;
                        }
                        
                        // читаем номер блока
                        var blockId = new byte[Block.HeaderBlockIdLength];
                        read = streamIn.Read(blockId, 0, blockId.Length);
                        if (read > 0)
                        {
                            int id = BitConverter.ToInt32(blockId, 0);

                            // читаем размер сжатого блока
                            var blockSize = new byte[Block.HeaderBlockSizeLength];
                            read = streamIn.Read(blockSize, 0, blockSize.Length);
                            if (read > 0)
                            {
                                // читаем данные
                                var length = BitConverter.ToInt32(blockSize, 0);
                                var data = new byte[length];
                                read = streamIn.Read(data, 0, length);

                                proceedBlocks[id] = BlockStatus.Read;
                                lock (queueDecompress.SyncRoot)  queueDecompress.Enqueue(new Block(id, data, read));                                
#if DEBUG
                                log.Trace($"Read {id}) {streamIn.Name} ({read}) Поток чтения");
#endif
                            }
                        }
                    }
                    while (read > 0);
                    
                    foreach (var t in decompressThreads)
                    {
                        t.Join();
                    }

                    writeThread.Join();

                    if (!hasError && !_break && !cancel())
                    {
                        log.Trace($"Размер распакованного файла {streamOut.Length}");

                        exitCode = ExitCode.Success;
                    }
                }
                else
                {
                    throw new Exception($"Неверный формат количества блоков сжатого файла {streamIn.Name}");
                }
            }
            else
            {
                throw new Exception($"Неверный заголовок сжатого файла {streamIn.Name}");
            }

            return exitCode;
        }
        
        void ThreadDecompressing()
        {
            try
            {
                while (proceedBlocks.Any(x => x == BlockStatus.Read || x == BlockStatus.Init))
                {
                    if (_break || cancel())
                    {
                        log.Trace($"{Thread.CurrentThread.Name}: Принудительная остановка");
                        break;
                    }

                    Block block = null;
                    
                    lock (queueDecompress.SyncRoot)
                    {
                        if (queueDecompress.Count > 0)
                        {
                            block = queueDecompress.Dequeue() as Block;
                        }
                    }
                    
                    if (block != null)
                    {
                        using (GZipStream streamGZip = new GZipStream(new MemoryStream(block.Data), CompressionMode.Decompress))
                        {
                            byte[] buffer = new byte[BufferSize];
                            int size = streamGZip.Read(buffer, 0, BufferSize);

                            proceedBlocks[block.Id] = BlockStatus.Decompressed;
                            lock (_queueWritingSync) queueWriting.Add(block.Id, new Block(block.Id, buffer, size));                            
#if DEBUG
                            log.Trace($"Decompress {block.Id}) {streamOut.Name} ({block.Size}) {Thread.CurrentThread.Name}");
#endif
                        }
                    }
                }
            }
            catch (Exception e)
            {
                hasError = true;
                log.Fatal($"{Thread.CurrentThread.Name}: {e.Message}");
                log.Trace(e.StackTrace);
                Break();
            }
        }
        
        void ThreadWriting()
        {
            try
            {
                while (proceedBlocks.Any(x => x == BlockStatus.Decompressed || x == BlockStatus.Read || x == BlockStatus.Init))
                {
                    if (_break || cancel())
                    {
                        log.Trace($"{Thread.CurrentThread.Name}: Принудительная остановка");
                        break;
                    }

                    lock (_queueWritingSync)
                    {
                        if (queueWriting.Count > 0 && queueWriting.ContainsKey(blocksNext))
                        {
                            var block = queueWriting[blocksNext];
                            
                            proceedBlocks[block.Id] = BlockStatus.Written;
                            streamOut.Write(block.Data, 0, block.Size);
                            queueWriting.Remove(blocksNext);
                            Interlocked.Increment(ref blocksNext);
#if DEBUG
                            log.Trace($"Written {block.Id}) {streamOut.Name} ({block.Size}) {Thread.CurrentThread.Name}");
#endif
                            progress?.Invoke(Interlocked.Increment(ref blocksProceed), blocksCount);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                hasError = true;
                log.Fatal($"{Thread.CurrentThread.Name}: {e.Message}");
                log.Trace(e.StackTrace);
                Break();
            }
        }
    }
}
