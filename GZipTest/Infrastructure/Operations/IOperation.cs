﻿using System;

namespace GZipTest.Infrastructure.Operations
{
    public interface IOperation : IDisposable
    {
        ExitCode Run();
        Operation AddProgress(Action<long, long> _progress);
        Operation AddCancel(Func<bool> _cancel);
    }
}
