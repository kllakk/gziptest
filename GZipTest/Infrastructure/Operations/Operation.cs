﻿using NLog;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;

namespace GZipTest.Infrastructure.Operations
{
    public class Operation : IDisposable
    {
        bool disposed = false;

        protected const int BufferSize = 1048576;
        protected const string GZTFileHeaderId = "GZipTest";
        protected readonly int GZTFileHeaderBlocksSize = sizeof(int);

        protected volatile bool _break = false;
        protected Func<bool> cancel = () => false;
        protected Action<long, long> progress;

        protected FileInfo fileIn;
        protected FileInfo fileOut;
        protected FileStream streamIn;
        protected FileStream streamOut;
                
        protected static ILogger log = LogManager.GetCurrentClassLogger();
        protected ExitCode exitCode = ExitCode.Error;
        
        protected int processorsCount = Environment.ProcessorCount;

        protected int numberBlock = sizeof(long);
        protected int sizeBlock = sizeof(int);

        public Operation(FileInfo _fileIn, FileInfo _fileOut)
        {
            processorsCount = Helpers.NumbersOfCores();

            fileIn = _fileIn;
            fileOut = _fileOut;
            
            log.Trace($"Размер файла {fileIn.Length} байт");
            log.Trace($"Количество ядер процессоров {processorsCount}");

            streamIn = new FileStream(fileIn.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
            streamOut = new FileStream(fileOut.FullName, FileMode.Create, FileAccess.Write);
        }

        public static IOperation Create(CompressionMode compressionMode, FileInfo _fileIn, FileInfo _fileOut)
        {
            switch (compressionMode)
            {
                case CompressionMode.Compress:
                    return new Compress(_fileIn, _fileOut);
                case CompressionMode.Decompress:
                    return new Decompress(_fileIn, _fileOut);
            }
            
            return null;
        }

        public void Break()
        {
            _break = true;
        }

        public Operation AddProgress(Action<long, long> _progress)
        {
            progress = _progress;
            return this;
        }

        public Operation AddCancel(Func<bool> _cancel)
        {
            cancel = _cancel;
            return this;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // управляемые ресурсы
                
            }

            // неуправляемые ресурсы
            if (streamIn != null)
            {
                streamIn.Close();
                streamIn = null;
            }

            if (streamOut != null)
            {
                streamOut.Close();
                streamOut = null;
            }

            disposed = true;
        }

        ~Operation()
        {
            Dispose(false);
        }
    }
}
