﻿using System;
using System.IO;

namespace GZipTest.Infrastructure
{
    static class Helpers
    {
        public static int NumbersOfCores()
        {
            int numbersOfCores = 0;

            try
            {
                foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
                {
                    numbersOfCores += int.Parse(item["NumberOfCores"].ToString());
                }
            }
            catch
            {
                numbersOfCores = Environment.ProcessorCount;
            }

            return numbersOfCores;
        }

        public static bool TryParse<TEnum>(string value, out TEnum result) where TEnum : struct
        {
            try
            {
                result = (TEnum)Enum.Parse(typeof(TEnum), value, true);
            }
            catch
            {
                result = default(TEnum);
                return false;
            }

            return true;
        }

        public static bool FileNameIsValid(string inputPath)
        {
            char[] invalidFileChars = Path.GetInvalidFileNameChars();

            try
            {
                string fileName = Path.GetFileName(inputPath);
                if (fileName.IndexOfAny(invalidFileChars) >= 0)
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
