﻿using System;
using System.Linq;
using System.IO.Compression;
using System.IO;
using GZipTest.Infrastructure;
using GZipTest.Infrastructure.Operations;
using NLog;
using System.Diagnostics;

namespace GZipTest
{
    class Program
    {
        static bool _cancel = false;
        static CompressionMode compressionMode;
        static ILogger log = LogManager.GetCurrentClassLogger();
        static FileInfo fileIn;
        static FileInfo fileOut;
        static Stopwatch w = new Stopwatch();

        static int Main(string[] args)
        {
#if DEBUG
            if (args.Count() == 0)
            {
                args = new string[] { "compress", "..\\Release\\test.bin", "..\\Release\\test.bin.gzt" };
                //args = new string[] { "decompress", "h:\\test.bin.gzt", "h:\\test.gzt.bin" };
            }
            Console.WriteLine($"Время запуска {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}");
#endif
            ExitCode result = ExitCode.Error;
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                eventArgs.Cancel = true;
                _cancel = true;
                log.Info("Нажата комбинация клавишь Ctrl + C для отмены выполнения процесса");
            };

            try
            {
                w.Start();

                IsArgsValidate(args);

                using (var operation = Operation.Create(compressionMode, fileIn, fileOut))
                {
                    operation.AddProgress((i, t) => {
                        Console.Write($"\rВыполнено {(int)((i * 100) / t)}%{(i == t ? "\r\n" : string.Empty)}");
                    });
                    operation.AddCancel(() => {
                        return _cancel;
                    });
                    result = operation.Run();
                }

                w.Stop();
                log.Warn($"Выполнено за {w.Elapsed}");
            }
            catch (Exception e)
            {
                log.Fatal(e.Message);
                log.Trace(e.StackTrace);
            }
            finally
            {
            }

#if DEBUG
            log.Warn($"Код возврата {result} ({(int)result})");
            Console.WriteLine($"Время завершения {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}");
            Console.WriteLine("Для продолжения нажмите Enter...");
            Console.ReadLine();
#endif
            return (int)result;
        }

        static void IsArgsValidate(string[] args)
        {
            if (args.Count() != 3)
            {
                throw new ArgumentException("Некорректное количество аргументов" + helpText);
            }

            if (!Helpers.TryParse(args[0], out CompressionMode _compressionMode))
            {
                throw new ArgumentException($"Первый аргумент может принимать только два значения compress и decompress" + helpText);
            }

            compressionMode = _compressionMode;

            if (!Helpers.FileNameIsValid(args[1]))
            {
                throw new ArgumentException("Второй аргумент содержит недопустимые символы");
            }

            if (!Helpers.FileNameIsValid(args[2]))
            {
                throw new ArgumentException("Третий аргумент содержит недопустимые символы");
            }

            fileIn = new FileInfo(args[1]);
            fileOut = new FileInfo(args[2]);

            if (!fileIn.Exists)
            {
                throw new ArgumentException($"Файл источник { fileIn.FullName } не существует");
            }

            if (fileIn.Length == 0)
            {
                throw new ArgumentException($"Файл {fileIn.FullName} имеет нулевой размер");
            }

            if (fileIn.FullName == fileOut.FullName)
            {
                throw new ArgumentException("Одинаковые имена файлов входного и выходного файла");
            }

            if (fileOut.Exists)
            {
                fileOut.Delete();
                fileOut.Refresh();

                if (fileOut.Exists)
                {
                    throw new ArgumentException($"Файл {fileOut} существует и заблокирован другим процессом");
                }
            }
        }

        const string helpText = @"

---------------------------------------------
Использование: GZipTest [mode] [in] [out]

mode:
  compress - сжать файл
  decompress - распаковать файл

in: путь к исходному файлу
out: путь к результирующему файлу

Примеры:
gziptest compress file.xml file.xml.gzt
gziptest decompress file.xml.gzt file.gzt.xml
---------------------------------------------
";
    }
}
